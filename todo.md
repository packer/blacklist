- [-] Add `-config` to yaml file (loaded and verified via config.py).
- [X] Store AS as IP Networks in BlacklistIPv?Network Table
      - [X] Do performance check
- [-] Store Single IPs as IP Networks in BlacklistIPv?Network Table
      - [-] Do performance check
- [X] Remove ip range via CIDR notation
      - Entries are removed if IP is hit
- [X] Split add into individual _add* functions?
- [-] Add sum function that prints tototal sum of blocked ip addreses
- [X] Only check IP on the very first join (is_joined? check xanmel key)
- [X] Support gzip
- [X] Async requests
- [-] Asnyc sqlite / aiosqlite / or use peewee-async, because xanmel…
- [X] Improve file paths (better symlink)
- [X] Split db (and models) into database.py
- [X] Return true/false if ban was successfull -> Server needs to execute rcon
      We do not have to submit a ban to banlist, because server does it anyway.
- [X] Add blacklist.py?
- [X] Properly support ipv6
- [X] Browse ipv4 table if ipv4, browse 6 if 6
- [X] Use exists() instead of count()
- [X] Add commandline interface to __init__.py
	- [X] Remove/Create blacklist
	- [X] Remove single ip from blacklist and banlist
