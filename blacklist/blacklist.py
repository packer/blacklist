import ipaddress
import gzip
import logging
import os
import time
from typing import Tuple, Union
from .typing import IPAddress, IPNetwork
from .asn import ASN
from .config import listsdir
from .database import Database
from .util import (
    CIDR,
    ipv6_prefix,
    ipv6_packedprefix,
    ipv6_from_packedprefix,
    ipv6_from_packedprefix_high,
)

logger = logging.getLogger(__name__)


class Blacklist:
    @staticmethod
    def drop() -> None:
        Database.drop()
        Database.init()
        Blacklist.stats()

    @staticmethod
    def build() -> None:
        Blacklist.populate()
        Blacklist.stats()

    @staticmethod
    def add(entry: str = "") -> None:
        """
        Add one of the following to the blacklist:
            - IP address
            - IP Network (CIDR notation)
            - AS number
        """
        Database.init()
        try:
            Blacklist._add(entry)
            Database.commit()
        except Exception as e:
            raise e

    @staticmethod
    def add_iprange(first: str, last: str) -> None:
        """
        Add an IP range to the blacklist.
        """
        Database.init()
        try:
            Blacklist._add_iprange(first, last)
            Database.commit()
        except Exception as e:
            raise e

    @staticmethod
    def _add(entry: str) -> None:
        if entry.startswith("AS"):
            Blacklist._add_asn(entry)
        elif "/" not in entry:
            Blacklist._add_address(entry)
        else:  # '/' in entry
            Blacklist._add_network(entry)

    @staticmethod
    def _add_asn(asn: str):
        prefixes = ASN.lookup_prefixes(asn)
        if prefixes is not None:
            for prefix in prefixes:
                Blacklist._add_network(prefix)
            logger.debug(f"Added {asn}")
        else:
            logger.warning(f"{asn} not found")

    @staticmethod
    def _add_address(ip_addr: Union[str, IPAddress]) -> None:
        if isinstance(ip_addr, str):
            try:
                ip_addr = ipaddress.ip_address(ip_addr)
            except Exception as e:
                raise e

        if ip_addr.version == 4:
            Database.add_ipv4_address(ip_addr.packed)
            logger.debug(f"Added {ip_addr} to ipv4_address")
        else:
            Database.add_ipv6_address(ipv6_packedprefix(ip_addr))
            ip_addr = ipv6_prefix(ip_addr)
            logger.debug(f"Added {ip_addr}/64 to ipv6_address")

    @staticmethod
    def _add_network(ip_network: Union[str, IPNetwork]) -> None:
        if isinstance(ip_network, str):
            try:
                ip_network = ipaddress.ip_network(ip_network)
            except Exception as e:
                raise e

        network_addr = ip_network.network_address
        broadcast_addr = ip_network.broadcast_address

        if ip_network.version == 4:
            Database.add_ipv4_network(network_addr.packed, broadcast_addr.packed)
            logger.debug(f"Added {ip_network} to ipv4_network")
        else:
            if ip_network.prefixlen < 64:
                Database.add_ipv6_network(
                    ipv6_packedprefix(network_addr), ipv6_packedprefix(broadcast_addr)
                )
                logger.debug(f"Added {ip_network} to ipv6_network")
            else:
                Database.add_ipv6_address(ipv6_packedprefix(network_addr))
                network_addr = ipv6_prefix(network_addr)
                logger.debug(f"Added {network_addr}/64 to ipv6_address")

    @staticmethod
    def _add_iprange(first: str, last: str) -> None:
        for network in CIDR.from_iprange(first, last):
            Blacklist._add_network(network)

    @staticmethod
    def remove(ip_addr: str) -> None:
        """
        Removes an IP from blacklist.
        All entries containing this IP are removed.
        """
        Database.init()
        try:
            Blacklist._remove(ip_addr)
            Database.commit()
        except Exception as e:
            raise e

    @staticmethod
    def _remove(ip_addr: str) -> None:
        try:
            ip_addr = ipaddress.ip_address(ip_addr)
        except Exception as e:
            raise e

        deleted = False

        # Check IP network:
        if ip_addr.version == 4:
            if logger.root.level <= logging.DEBUG:
                for network, broadcast in Database.delete_ipv4_network(ip_addr.packed):
                    deleted = True
                    network_addr = ipaddress.ip_address(network)
                    broadcast_addr = ipaddress.ip_address(broadcast)
                    network = next(CIDR.from_iprange(network_addr, broadcast_addr))
                    logger.debug(f"Removed {network} from ipv4_network")
            else:
                for network, broadcast in Database.delete_ipv4_network(ip_addr.packed):
                    deleted = True
                    pass
        else:
            if logger.root.level <= logging.DEBUG:
                for network, broadcast in Database.delete_ipv6_network(
                    ipv6_packedprefix(ip_addr)
                ):
                    deleted = True
                    network_addr = ipv6_from_packedprefix(network)
                    broadcast_addr = ipv6_from_packedprefix_high(broadcast)
                    network = next(CIDR.from_iprange(network_addr, broadcast_addr))
                    logger.debug(f"Removed {network} from ipv6_network")
            else:
                for network, broadcast in Database.delete_ipv6_network(
                    ipv6_packedprefix(ip_addr)
                ):
                    deleted = True
                    pass

        # Check IP address
        if ip_addr.version == 4:
            delete = Database.delete_ipv4_address(ip_addr.packed)
            if delete:
                logger.debug(f"Removed {ip_addr} from ipv4_address")
                deleted = True
        else:
            delete = Database.delete_ipv6_address(ipv6_packedprefix(ip_addr))
            if delete:
                ip_addr = ipv6_prefix(ip_addr)
                logger.debug(f"Removed {ip_addr}/64 from ipv6_address")
                deleted = True

        if not deleted:
            logger.info(f"Could not find {ip_addr} on any list.")

    @staticmethod
    def populate() -> None:
        def _parse(fd) -> None:
            logger.info(f"Parsing {fd.name}")
            for i, entry in enumerate(fd):

                if entry.startswith("#"):
                    continue

                entry = entry.strip()
                if entry:
                    if len(entry.split(" ")) == 2:
                        try:
                            first, last = entry.split(" ")
                            Blacklist._add_iprange(first, last)
                        except ValueError as e:
                            logger.warning(e)
                            continue
                    else:
                        try:
                            Blacklist._add(entry)
                        except ValueError as e:
                            logger.warning(e)
                            continue

        Database.init()
        start = time.monotonic()
        blacklists = []
        for cur_path, dirs, files in os.walk(listsdir):
            for f in files:
                if f == "example.txt":
                    continue
                blacklists.append(os.path.join(cur_path, f))

        for src in sorted(blacklists):
            ext = os.path.splitext(src)[1]
            if ext == ".gz":
                with gzip.open(src, "rt") as fd:
                    _parse(fd)
            else:
                with open(src, "r") as fd:
                    _parse(fd)

        logger.info("Parsing of lists finished. Committing...")
        Database.commit()

        Database.vacuum()
        duration = time.monotonic() - start
        logger.info(f"Populated database and called `VACUUM` in {duration:.2f} sec")

    @staticmethod
    def stats():
        def _row(c: tuple):
            logger.info(f"{c[0]:<30} | {c[1]:>6}")

        # Header
        logger.info(50 * "-")
        _row(c=("Tablename", "Entries"))
        logger.info(50 * "-")

        Database.init()
        # Table data:
        for table in "ipv4_address", "ipv4_network", "ipv6_address", "ipv6_network":
            count = Database.count_rows(table)
            _row((table, count))

    @staticmethod
    def lookup(ip_addr: str) -> Tuple[bool, str]:
        Database.init()

        start = time.monotonic()
        logger.debug(f"Looking for IP address: {ip_addr}")

        try:
            ip_addr = ipaddress.ip_address(ip_addr)
        except ValueError as e:
            raise e

        if ip_addr.version == 4:
            packed = ip_addr.packed

            found = Database.lookup_ipv4_network(packed)
            if found:
                network_addr, broadcast_addr = found
                network_addr = ipaddress.IPv4Address(network_addr)
                broadcast_addr = ipaddress.IPv4Address(broadcast_addr)
                network = next(CIDR.from_iprange(network_addr, broadcast_addr))
                logger.info(f"IP address found in ipv4_network: {network}")
                end = time.monotonic()
                logger.debug(f"Database checked in {1000*(end-start):.1f} ms")
                return (True, "IPv4 Network")

            found = Database.lookup_ipv4_address(packed)
            if found:
                logger.info(f"IP address was found in ipv4_address: {ip_addr}")
                end = time.monotonic()
                logger.debug(f"Database checked in {1000*(end-start):.1f} ms")
                return (True, "IPv4 Address")
        else:
            packed = ipv6_packedprefix(ip_addr)

            found = Database.lookup_ipv6_network(packed)
            if found:
                network, broadcast = found
                network_addr = ipv6_from_packedprefix(network)
                broadcast_addr = ipv6_from_packedprefix_high(broadcast)
                network = next(CIDR.from_iprange(network_addr, broadcast_addr))
                logger.info(f"IP address was found in ipv6_network: {network}")
                end = time.monotonic()
                logger.debug(f"Database checked in {1000*(end-start):.1f} ms")
                return (True, "IPv6 Network")

            found = Database.lookup_ipv6_address(packed)
            if found:
                logger.info(
                    f"IP address was found in ipv6_address: {ipv6_prefix(ip_addr)}/64"
                )
                end = time.monotonic()
                logger.debug(f"Database checked in {1000*(end-start):.1f} ms")
                return (True, "IPv6 Address")

        end = time.monotonic()
        logger.debug(f"IP not listed. Database checked in {1000*(end-start):.1f} ms")
        return (False, None)


# vim:sw=4:ts=4:et:
