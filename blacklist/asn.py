import ipaddress
import logging
import pyasn
import time
from typing import Tuple, List

from .config import asnfile

logger = logging.getLogger(__name__)


class ASN:

    db = None

    @staticmethod
    def _load() -> None:
        if ASN.db is None:
            try:
                logger.info("Loading ASN database...")
                start = time.monotonic()
                ASN.db = pyasn.pyasn(asnfile)
                end = time.monotonic()
                logger.info(f"Loaded ASN database ({1000*(end-start):.2f} ms)")
            except (ValueError, FileNotFoundError) as e:
                raise e

    @staticmethod
    def lookup(ip_addr: str) -> Tuple[str, str]:
        try:
            ip_addr = ipaddress.ip_address(ip_addr)
        except ValueError as e:
            raise e

        ASN._load()
        return ASN.db.lookup(str(ip_addr))

    @staticmethod
    def lookup_prefixes(asn: str) -> List[str]:
        if asn.startswith("ASN"):
            asn = asn[3:]
        elif asn.startswith("AS"):
            asn = asn[2:]
        try:
            asn = int(asn)
        except Exception:
            raise Exception(f"Bad AS number: {asn}")

        ASN._load()
        return ASN.db.get_as_prefixes_effective(asn)


# vim:sw=4:ts=4:et:
