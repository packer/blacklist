import ipaddress
from typing import Tuple, List, ByteString, Union, Iterator
from .typing import IPAddress, IPNetwork


def ipv6_prefix(address: ipaddress.IPv6Address) -> ipaddress.IPv6Address:
    """
    Return IPv6Address with all interface bits set to zero.
    """
    return ipaddress.IPv6Address(int(address) & ~0xFFFFFFFFFFFFFFFF)
    # 0xFFFFFFFFFFFFFFFF is ((1 << 64) - 1))


def ipv6_packedprefix(address: ipaddress.IPv6Address) -> ByteString:
    """
    Return 'routing prefix + subnet ID' as bytestring.
    """
    return int.to_bytes(int(address) >> 64, 8, "big")


def ipv6_from_packedprefix(packed_prefix: ByteString) -> ipaddress.IPv6Address:
    """
    Return IPv6Address from 'routing prefix + subnet' bytestring, interface
    bits are zero.
    """
    return ipaddress.IPv6Address(int.from_bytes(packed_prefix, "big") << 64)


def ipv6_from_packedprefix_high(packed_prefix: ByteString) -> ipaddress.IPv6Address:
    """
    Return IPv6Address from 'routing prefix + subnet' bytestring, interface
    bits are set (one).
    """
    return ipaddress.IPv6Address(
        (int.from_bytes(packed_prefix, "big") << 64) | 0xFFFFFFFFFFFFFFFF
    )


class CIDR:
    @staticmethod
    def to_iprange(ip_network: Union[str, IPNetwork]) -> Tuple[IPAddress, IPAddress]:
        if isinstance(ip_network, str):
            ip_network = ipaddress.ip_network(ip_network)
        network_addr = ip_network.network_address
        broadcast_addr = ip_network.broadcast_address
        return (network_addr, broadcast_addr)

    @staticmethod
    def from_iprange(
        first: Union[str, IPAddress], last: Union[str, IPAddress]
    ) -> Iterator[IPNetwork]:
        if isinstance(first, str):
            first = ipaddress.ip_address(first)
        if isinstance(last, str):
            last = ipaddress.ip_address(last)
        for network in ipaddress.summarize_address_range(first, last):
            yield network

    @staticmethod
    def from_list(*args: str) -> Iterator[IPNetwork]:
        def address_or_network(i):
            if "/" not in i:
                return ipaddress.ip_network(i)
            return ipaddress.ip_network(i)

        addresses = (address_or_network(i) for i in args)
        for network in ipaddress.collapse_addresses(addresses):
            yield network


# vim:sw=4:ts=4:et:
