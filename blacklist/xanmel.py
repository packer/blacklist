import asyncio
import logging
from typing import Tuple
from .banlist import Banlist
from .blacklist import Blacklist

logger = logging.getLogger(__name__)


async def check_and_ban(ip: str, reason: str) -> bool:
    found, blacklist_reason = Blacklist.lookup(ip)

    if found:
        reason = f"{blacklist_reason} {reason}"
        await Banlist.add(ip, reason)
        return True

    return False


# vim:sw=4:ts=4:et:
