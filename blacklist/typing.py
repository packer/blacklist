import ipaddress
from typing import TypeVar

IPAddress = TypeVar("IPAddress", ipaddress.IPv4Address, ipaddress.IPv6Address)
IPNetwork = TypeVar("IPNetwork", ipaddress.IPv4Network, ipaddress.IPv6Network)
