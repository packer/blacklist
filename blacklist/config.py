import os
import yaml

try:
    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.abspath(os.path.join(path, "../"))
    with open(os.path.join(path, "config.yaml"), "r") as fd:
        try:
            data = yaml.safe_load(fd)
        except Exception as e:
            print(f"Broken configuration file:\n{e}")
            exit(11)
except FileNotFoundError as e:
    print(f"Configuration file not found:\n{e}")
    exit(13)

banlist = data.get("banlist")
sqlitefile = data.get("sqlitefile")
asnfile = data.get("asnfile")
listsdir = data.get("listsdir")

# Defaults:
if not sqlitefile:
    sqlitefile = "blacklist.sqlite"
if not asnfile:
    asnfile = "pyasn/pyasn.dat.gz"
if not listsdir:
    listsdir = "lists/"

# vim:sw=4:ts=4:et:
