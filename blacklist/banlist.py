import aiohttp
import asyncio
import logging
from .config import banlist as url

logger = logging.getLogger(__name__)


class Banlist:
    @staticmethod
    async def submit(data: dict) -> bool:
        async with aiohttp.ClientSession() as session:
            logger.info(f"Sending action={data['action']} request to banlist")
            async with session.get(url, params=data) as resp:
                if resp.status == 200:
                    return True
                return False

    @staticmethod
    async def add(ip: str, reason: str = "") -> None:
        if not url:
            logger.warning("Cannot submit request to banlist. Banlist not defined.")
            return
        data = {
            "action": "ban",
            "hostname": "BLACKLIST",
            "duration": 24 * 3600,
            "ip": ip,
            "reason": reason,
        }
        ret = await Banlist.submit(data)
        if ret is True:
            logger.info(f"Added ban for {ip}")
        else:
            logger.warning(f"Failed to add ban for {ip}. Banlist down?")

    @staticmethod
    async def remove(ip: str) -> None:
        if not url:
            logger.warning("Cannot submit request to banlist. Banlist not defined.")
            return
        data = {"action": "unban", "hostname": "BLACKLIST", "ip": ip}
        ret = await Banlist.submit(data)
        if ret is True:
            logger.info(f"Removed ban for {ip}")
        else:
            logger.warning(f"Failed to remove ban for {ip}. Banlist down?")

    @staticmethod
    def add_noasync(ip: str, reason: str = "") -> bool:
        return asyncio.run(Banlist.add(ip, reason))

    @staticmethod
    def remove_noasync(ip: str) -> bool:
        return asyncio.run(Banlist.remove(ip))


# vim:sw=4:ts=4:et:
