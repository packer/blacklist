import logging
import sqlite3
from typing import ByteString, Tuple, Iterator
from .config import sqlitefile

logger = logging.getLogger(__name__)


class Database:

    conn = sqlite3.connect(sqlitefile, timeout=0.1)
    cursor = conn.cursor()

    @classmethod
    def init(cls) -> None:
        cls.cursor.executescript(
            """
            CREATE TABLE IF NOT EXISTS ipv4_address (
            id          INTEGER         PRIMARY KEY AUTOINCREMENT,
            address     BINARY(4)       NOT NULL UNIQUE ON CONFLICT IGNORE);

            CREATE INDEX IF NOT EXISTS
                idx_ipv4_address_address ON ipv4_address(address);

            CREATE TABLE IF NOT EXISTS ipv4_network (
            id          INTEGER         PRIMARY KEY AUTOINCREMENT,
            network     BINARY(4)       NOT NULL,
            broadcast   BINARY(4)       NOT NULL,
            UNIQUE(network, broadcast) ON CONFLICT IGNORE);

            CREATE INDEX IF NOT EXISTS
                idx_ipv4_network_network ON ipv4_network(network);
            CREATE INDEX IF NOT EXISTS
                idx_ipv4_network_broadcast ON ipv4_network(broadcast);
            CREATE INDEX IF NOT EXISTS
                idx_ipv4_network_networkbroadcast ON ipv4_network(network, broadcast);

            CREATE TABLE IF NOT EXISTS ipv6_address (
            id          INTEGER         PRIMARY KEY AUTOINCREMENT,
            address     BINARY(8)       NOT NULL UNIQUE ON CONFLICT IGNORE);

            CREATE INDEX IF NOT EXISTS
                idx_ipv6_address_address ON ipv6_address(address);

            CREATE TABLE IF NOT EXISTS ipv6_network (
            id          INTEGER         PRIMARY KEY AUTOINCREMENT,
            network     BINARY(8)       NOT NULL,
            broadcast   BINARY(8)       NOT NULL,
            UNIQUE(network, broadcast)  ON CONFLICT IGNORE);

            CREATE INDEX IF NOT EXISTS
                idx_ipv6_network_network ON ipv6_network(network);
            CREATE INDEX IF NOT EXISTS
                idx_ipv6_network_broadcast ON ipv6_network(broadcast);
            CREATE INDEX IF NOT EXISTS
                idx_ipv6_network_networkbroadcast ON ipv6_network(network, broadcast);
        """
        )
        cls.conn.commit()

    @classmethod
    def drop(cls) -> None:
        cls.cursor.executescript(
            """
            DROP TABLE IF EXISTS ipv4_address;
            DROP TABLE IF EXISTS ipv4_network;
            DROP TABLE IF EXISTS ipv6_address;
            DROP TABLE IF EXISTS ipv6_network;
        """
        )
        cls.conn.commit()

    @classmethod
    def vacuum(cls) -> None:
        cls.conn.execute("VACUUM;")
        cls.conn.commit()

    @classmethod
    def commit(cls) -> None:
        cls.conn.commit()

    @classmethod
    def count_rows(cls, table: str) -> int:
        count = cls.cursor.execute(f"SELECT COUNT(id) FROM {table};").fetchone()
        return count[0] if count else 0

    # fmt: off

    @classmethod
    def add_ipv4_address(cls, address: ByteString) -> None:
        cls.cursor.execute("""
            INSERT INTO ipv4_address(address) VALUES(?);
            """, (address,))

    @classmethod
    def add_ipv4_network(cls, network: ByteString, broadcast: ByteString) -> None:
        cls.cursor.execute("""
            INSERT INTO ipv4_network(network, broadcast) VALUES(?, ?);
            """, (network, broadcast))

    @classmethod
    def lookup_ipv4_address(cls, address: ByteString) -> ByteString:
        result = cls.cursor.execute("""
            SELECT address FROM ipv4_address
            WHERE address = ?
            LIMIT 1;
            """, (address,)).fetchone()
        return result[0] if result else None

    @classmethod
    def lookup_ipv4_network(cls, address: ByteString) -> Tuple[ByteString, ByteString]:
        result = cls.cursor.execute("""
            SELECT network, broadcast FROM ipv4_network
            WHERE network <= ? AND broadcast >= ?
            LIMIT 1;
            """, (address, address)).fetchone()
        return result if result else None

    @classmethod
    def delete_ipv4_address(cls, address: ByteString) -> ByteString:
        row = cls.cursor.execute("""
            SELECT * FROM ipv4_address
            WHERE address = ?;
            """, (address,)).fetchone()
        if row:
            id, address = row
            cls.cursor.execute("""
                DELETE FROM ipv4_address WHERE id = ?;
                """, (id,))
        return address if row else None

    @classmethod
    def delete_ipv4_network(
        cls, address: ByteString
    ) -> Iterator[Tuple[ByteString, ByteString]]:
        for row in cls.cursor.execute("""
                SELECT * FROM ipv4_network
                WHERE network <= ? AND broadcast >= ?;
                """, (address, address)).fetchall():
            id, network, broadcast = row
            cls.cursor.execute("""
                DELETE FROM ipv4_network WHERE id = ?;
                """, (id,))
            yield (network, broadcast)

    @classmethod
    def add_ipv6_address(cls, address: ByteString) -> None:
        cls.cursor.execute("""
            INSERT INTO ipv6_address(address) VALUES(?);
            """, (address,))

    @classmethod
    def add_ipv6_network(cls, network: ByteString, broadcast: ByteString) -> None:
        cls.cursor.execute("""
            INSERT INTO ipv6_network(network, broadcast) VALUES(?, ?);
            """, (network, broadcast))

    @classmethod
    def lookup_ipv6_address(cls, address: ByteString) -> ByteString:
        result = cls.cursor.execute("""
            SELECT address FROM ipv6_address
            WHERE address = ?
            LIMIT 1;
            """, (address,)).fetchone()
        return result[0] if result else None

    @classmethod
    def lookup_ipv6_network(cls, address: ByteString) -> Tuple[ByteString, ByteString]:
        result = cls.cursor.execute("""
            SELECT network, broadcast FROM ipv6_network
            WHERE network <= ? AND broadcast >= ?
            LIMIT 1;
            """, (address, address)).fetchone()
        return result if result else None

    @classmethod
    def delete_ipv6_address(cls, address: ByteString) -> ByteString:
        row = cls.cursor.execute("""
            SELECT * FROM ipv6_address
            WHERE address = ?;
            """, (address,)).fetchone()
        if row:
            id, address = row
            cls.cursor.execute("""
                DELETE FROM ipv6_address WHERE id = ?;
                """, (id,))
        return address if row else None

    @classmethod
    def delete_ipv6_network(
        cls, address: ByteString
    ) -> Iterator[Tuple[ByteString, ByteString]]:
        for row in cls.cursor.execute("""
                SELECT * FROM ipv6_network
                WHERE network <= ? AND broadcast >= ?;
                """, (address, address)).fetchall():
            id, network, broadcast = row
            cls.cursor.execute("""
                DELETE FROM ipv6_network WHERE id = ?;
                """, (id,))
            yield (network, broadcast)

# vim:sw=4:ts=4:et:
