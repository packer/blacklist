import argparse
import logging

from .asn import ASN
from .banlist import Banlist
from .blacklist import Blacklist
from .util import CIDR

# FORMAT = '[%(asctime)s] %(name)s %(levelname)s: %(message)s'
FORMAT = "%(message)s"
logger = logging.basicConfig(format=FORMAT, datefmt="%H:%M:%S", level="INFO")
logger = logging.getLogger(__name__)


def blacklist():
    parser = argparse.ArgumentParser(description="Blacklist Utility...")
    parser.add_argument(
        "-verbose", "-v", action="count", default=0, help="Verbose flag: Enable 'debug'"
    )
    parser.add_argument("-build", action="store_true", help="rebuild database")
    parser.add_argument(
        "-clear", action="store_true", help="remove all entries from blacklist"
    )
    parser.add_argument("-stats", action="store_true", help="print database stats")
    parser.add_argument(
        "-lookup",
        type=str,
        nargs=1,
        help="check if an address is in blacklist (returns first hit)",
    )
    parser.add_argument(
        "-add",
        type=str,
        nargs=1,
        help="add an entry to blacklist (IP address, IP network, or ASN)",
    )
    parser.add_argument(
        "-add-iprange", type=str, nargs=2, help="add an address range to blacklist"
    )
    parser.add_argument(
        "-remove", type=str, nargs=1, help="remove an entry from blacklist"
    )
    parser.add_argument(
        "-banlist-remove", type=str, nargs=1, help="remove an entry from banlist"
    )
    parser.add_argument(
        "-asn-lookup",
        type=str,
        nargs=1,
        help="lookup AS number and subnet for given address",
    )
    parser.add_argument(
        "-asn-prefixes", type=str, nargs=1, help="lookup prefixes for given AS number"
    )
    parser.add_argument(
        "-cidr-to-iprange",
        type=str,
        nargs=1,
        help="translate CIDR notation to address range",
    )
    parser.add_argument(
        "-cidr-from-iprange",
        type=str,
        nargs=2,
        help="translate address range to CIDR notation",
    )
    parser.add_argument(
        "-cidr-from-list",
        type=str,
        nargs="+",
        help="translate list of addresses and networks to CIDR notation",
    )

    args = parser.parse_args()

    if args.verbose > 0:
        logging.getLogger().setLevel("DEBUG")

    if args.build:
        try:
            Blacklist.build()
        except FileNotFoundError as e:
            print(f"Error: {e}")

    if args.clear:
        Blacklist.drop()

    if args.stats:
        Blacklist.stats()

    if args.lookup:
        try:
            Blacklist.lookup(*args.lookup)
        except ValueError as e:
            print(f"Error: {e}")

    if args.asn_lookup:
        try:
            asn = ASN.lookup(*args.asn_lookup)
            if asn[0]:
                asn, subnet = asn
                print(f"AS{asn}, {subnet}")
            else:
                print("Nothing found.")
        except (ValueError, FileNotFoundError) as e:
            print(f"Error: {e}")

    if args.asn_prefixes:
        try:
            prefixes = ASN.lookup_prefixes(*args.asn_prefixes)
            print(prefixes)
        except (ValueError, FileNotFoundError) as e:
            print(f"Error: {e}")

    if args.add:
        try:
            Blacklist.add(*args.add)
        except ValueError as e:
            print(f"Error: {e}")

    if args.add_iprange:
        try:
            Blacklist.add_iprange(*args.add_iprange)
        except ValueError as e:
            print(f"Error: {e}")

    if args.remove:
        try:
            Blacklist.remove(*args.remove)
        except ValueError as e:
            print(f"Error: {e}")

    if args.banlist_remove:
        Banlist.remove_noasync(*args.banlist_remove)

    if args.cidr_to_iprange:
        try:
            network_addr, broadcast_addr = CIDR.to_iprange(*args.cidr_to_iprange)
            print(f"{network_addr} to {broadcast_addr}")
        except ValueError as e:
            print(f"Error: {e}")

    if args.cidr_from_iprange:
        try:
            network_addr, broadcast_addr = args.cidr_from_iprange
            networks = CIDR.from_iprange(network_addr, broadcast_addr)
            for n in networks:
                print(n)
        except ValueError as e:
            print(f"Error: {e}")

    if args.cidr_from_list:
        try:
            networks = CIDR.from_list(*args.cidr_from_list)
            for n in networks:
                print(n)
        except (ValueError, TypeError) as e:
            print(f"Error: {e}")


# vim:sw=4:ts=4:et:
