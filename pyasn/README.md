### Download and convert IPASN Data Files

- https://github.com/hadiasghari/pyasn#ipasn-data-files

**Note:**
Execute these commands when `pyasn` is installed.
Then the required python scripts should exist in your environment.


```sh
# Example:
pyasn_util_download.py --latestv46
pyasn_util_convert.py --compress --single rib.20200503.0800.bz2 pyasn.dat
```
