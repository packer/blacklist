# Blacklist

A blacklist plugin for [xanmel](https://github.com/nsavch/xanmel)

Xanmel's Xonotic JOIN handler must be modified:

```py
	from .blacklist.blacklist.xanmel import check_and_ban
	from .colors import Color

	---

        # Check if IP is in blacklist…
        nickname = Color.dp_to_none(player.nickname).decode('utf8')
        reason = f"affects {nickname}"
        on_blacklist = await check_and_ban(player.ip_address, reason)
        if on_blacklist:
            server.send('defer 2 "kickban #%(slot)s %(time)s %(reason)s"' % {
                'slot': player.slot,
                'time': 60,
                'reason': "(Blacklist)"
                })
        # end of blacklist
```

Country blocks:
- https://ipdeny.com/ipblocks/

Some lookup tools:
- https://myip.ms/
- https://scamalytics.com/
- https://apility.io/


Note: Blacklist database lookups are not asynchronous!
