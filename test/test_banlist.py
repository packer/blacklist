import time

from blacklist.banlist import Banlist


def test_banlist():
    # Note: This sends real requests!
    Banlist.add_noasync("127.0.0.1", "Gitlab CI")
    Banlist.add_noasync("fe80::1", "Gitlab CI")
    time.sleep(5)
    Banlist.remove_noasync("127.0.0.1")
    Banlist.remove_noasync("fe80::1")
