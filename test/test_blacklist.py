import pytest
from blacklist.blacklist import Blacklist
from test.conftest import cleanup


def test_build():
    Blacklist.build()


def test_drop():
    Blacklist.drop()


def test_add():
    Blacklist.add("10.0.0.255")
    Blacklist.add("10.0.0.0/16")

    Blacklist.add("2300:2300:f:f:1:1:1:ffff")
    Blacklist.add("2300:2300::/48")

    with pytest.raises(FileNotFoundError) as excinfo:
        Blacklist.add("AS9009")
        assert "No such file or directory" in str(excinfo.value)



def test_lookup():
    found, reason = Blacklist.lookup("10.0.0.255")
    assert found == True

    found, reason = Blacklist.lookup("10.1.0.0")
    assert found == False

    found, reason = Blacklist.lookup("2300:2300:f:f:1:1:ffff::")
    assert found == True

    # Same /64 prefix:
    found, reason = Blacklist.lookup("2300:2300:f:f:aaaa::")
    assert found == True

    # Same /48 network
    found, reason = Blacklist.lookup("2300:2300:0:0:aaaa::")
    assert found == True

    # Does not exist
    found, reason = Blacklist.lookup("ffff:2300:0:0:f:f::1")
    assert found == False

def test_remove():
    Blacklist.remove("10.0.0.0")

    found, reason = Blacklist.lookup("10.1.0.0")
    assert found == False

    found, reason = Blacklist.lookup("10.0.0.255")
    assert found == True

    # Remove /64 prefix
    Blacklist.remove("2300:2300:f:f::1")

    found, reason = Blacklist.lookup("2300:2300:f:f::1")
    assert found == False

    # Same /64 prefix:
    found, reason = Blacklist.lookup("2300:2300:f:f:aaaa::")
    assert found == False

    # Same /48 network
    found, reason = Blacklist.lookup("2300:2300:0:0:aaaa::")
    assert found == True
