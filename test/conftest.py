import pytest
import os


@pytest.fixture(scope="session", autouse=True)
def cleanup(request):
    def remove_db():
        try:
            os.remove("blacklist.sqlite")
        except IOError:
            pass

    request.addfinalizer(remove_db)
