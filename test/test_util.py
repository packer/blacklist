import ipaddress
from blacklist.util import (
    ipv6_prefix,
    ipv6_packedprefix,
    ipv6_from_packedprefix,
    ipv6_from_packedprefix_high,
    CIDR,
)


def test_ipv6():
    address = ipaddress.IPv6Address("234a:4899:4353:8439:8a4f:80ab:da93:ce8f")

    expect = ":".join(str(address).split(":")[:4]) + "::"
    assert expect == str(ipv6_prefix(address))

    expect = b"\x23\x4a\x48\x99\x43\x53\x84\x39"
    assert expect == ipv6_packedprefix(address)

    expect = ":".join(str(address).split(":")[:4]) + "::"
    assert expect == str(ipv6_from_packedprefix(ipv6_packedprefix(address)))

    expect = ":".join(str(address).split(":")[:4]) + ":ffff:ffff:ffff:ffff"
    assert expect == str(ipv6_from_packedprefix_high(ipv6_packedprefix(address)))


def test_cidr():
    network_str = "10.0.0.0/16"
    network = ipaddress.ip_network(network_str)

    expect = (ipaddress.IPv4Address("10.0.0.0"), ipaddress.IPv4Address("10.0.255.255"))
    assert expect == CIDR.to_iprange(network_str)
    assert expect == CIDR.to_iprange(network)

    low, high = CIDR.to_iprange(network_str)
    expect = network
    assert expect == next(CIDR.from_iprange(low, high))
    assert expect == next(CIDR.from_iprange(str(low), str(high)))

    expect = network
    assert expect == next(CIDR.from_list("10.0.0.0/16", "10.0.0.255", "10.0.0.0/24"))

